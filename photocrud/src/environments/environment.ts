// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'photocrud',
    appId: '1:149687183312:web:4444f741f2b8af73bf3bd5',
    storageBucket: 'photocrud.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyB6EFCBfT2ZpdNITq31bP9gen20qgtwEgM',
    authDomain: 'photocrud.firebaseapp.com',
    messagingSenderId: '149687183312',
    measurementId: 'G-JKV3J0Q9KJ',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
