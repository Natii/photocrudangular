export default interface Photo { 
    id?: string;
    name: string;
    photo: string;
}