import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ref, Storage, uploadBytes } from '@angular/fire/storage';
import { WebcamImage } from 'ngx-webcam';
import { Observable, ReplaySubject, Subject } from 'rxjs';

@Component({
  selector: 'app-photo-create',
  templateUrl: './photo-create.component.html',
  styleUrls: ['./photo-create.component.css']
})
export class PhotoCreateComponent implements OnInit {

  formName: any = {
    name: ''
  }
  public webcam!: WebcamImage;
  private trigger: Subject<void> = new Subject<void>();

  constructor(private storage: Storage) {}

  ngOnInit(): void {}

  triggerSnapshot(): void {
    this.trigger.next();
  }

   handleImage(webcam: WebcamImage): void {
    console.info('Saved webcam image', webcam);
    this.webcam = webcam;
  }
    
   public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  uploadImage() {
    const { name } = this.formName
    console.log(this.webcam);
    const file = this.dataURLtoFile(this.webcam.imageAsDataUrl, name);
    
    const imgRef = ref(this.storage, `images/${file.name}`);

    uploadBytes(imgRef, file)
      .then(response => console.log(response))
      .catch(error => console.log(error));
  }

  dataURLtoFile(dataurl: any, filename:string) {
 
    let arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
        
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    return new File([u8arr], filename, {type:mime});
  }

}
