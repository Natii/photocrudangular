import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { getDownloadURL, listAll, ref, Storage, uploadBytes } from '@angular/fire/storage';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { Subject, Observable } from 'rxjs';

import Photo from 'src/app/interfaces/photo.interface';
import { PhotosService } from 'src/app/services/photos.service';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.css']
})
export class PhotoListComponent implements OnInit {

  photos: Photo[];
  images: string[];

  constructor(private photosServices: PhotosService, private storage: Storage) {
    this.photos = [{
      name: '',
      photo: ''
    }];
    this.images = [];
  }

  ngOnInit(): void {
    this.photosServices.getPhotos().subscribe(photos => {
      this.photos = photos;
    })
    this.getImages();
  }

  getImages() {
    const imagesRef = ref(this.storage, 'images');

    listAll(imagesRef)
      .then(async response => {
        console.log(response);
        this.images = [];
        for(let item of response.items) {
          const url = await getDownloadURL(item);
          this.images.push(url);
        }
      })
      .catch(error => console.log(error));
  }

  async onSubmitUpdatePhoto(photo: Photo) {
    const response = await this.photosServices.updatePhoto(photo);
    console.log(response);
  }

  async onClickDeletePhoto(photo: Photo) {
    const response = await this.photosServices.deletePhoto(photo);
    console.log(response);
  }
}
