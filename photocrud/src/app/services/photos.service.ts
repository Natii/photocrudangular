import { Injectable } from '@angular/core';
import { addDoc, collection, collectionData, doc, Firestore, deleteDoc, updateDoc } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import Photo from '../interfaces/photo.interface';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private firestore: Firestore) { }

  addPhoto(photo: Photo) { 
    const photoRef = collection(this.firestore, 'photo');
    return addDoc(photoRef, photo);
  }

  getPhotos(): Observable<Photo[]> {
    const photoRef = collection(this.firestore, 'photo');
    return collectionData(photoRef, { idField: 'id' }) as Observable<Photo[]>;
  }

  updatePhoto(photo: Photo) {
    const photoDocRef = doc(this.firestore, `photos/${photo.id}`);
    return updateDoc(photoDocRef, { idField: 'id' });
  }

  deletePhoto(photo: Photo) {
    const photoDocRef = doc(this.firestore, `photos/${photo.id}`);
    return deleteDoc(photoDocRef);
  }
}
