import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhotoCreateComponent } from './components/photo-create/photo-create.component';
import { PhotoListComponent } from './components/photo-list/photo-list.component';

const routes: Routes = [
  { path: '', component: PhotoListComponent },
  { path: 'photoCreate', component: PhotoCreateComponent },
  { path: 'photoList', component: PhotoListComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
